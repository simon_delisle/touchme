package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

public class GameMedium extends AppCompatActivity implements View.OnClickListener {

    private Button
            button10,
            button11,
            button12,
            button13,
            button14,
            button15,
            button16,
            button17,
            button18,
            button19,
            button20,
            button21,
            button22,
            button23,
            button24,
            buttonReadyM,
            buttonEndGameM;

    private EditText editTextPersonNameM;

    private TextView
            textViewTimeM,
            textViewScoreM ;

    private int score = 0;

    private Handler hr;
    private Thread tr;

    private int timeLeft = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_medium);

        buttonReadyM = (Button) findViewById(R.id.buttonReadyH);
        buttonEndGameM = (Button) findViewById(R.id.buttonEndGameH);
        button10 = (Button) findViewById(R.id.button10);
        button11 = (Button) findViewById(R.id.button11);
        button12 = (Button) findViewById(R.id.button12);
        button13 = (Button) findViewById(R.id.button13);
        button14 = (Button) findViewById(R.id.button14);
        button15 = (Button) findViewById(R.id.button15);
        button16 = (Button) findViewById(R.id.button16);
        button17 = (Button) findViewById(R.id.button17);
        button18 = (Button) findViewById(R.id.button18);
        button19 = (Button) findViewById(R.id.button19);
        button20 = (Button) findViewById(R.id.button20);
        button21 = (Button) findViewById(R.id.button21);
        button22 = (Button) findViewById(R.id.button22);
        button23 = (Button) findViewById(R.id.button23);
        button24 = (Button) findViewById(R.id.button24);

        //link edittext to the frontend
        editTextPersonNameM = (EditText) findViewById(R.id.editTextPersonNameH) ;

        //link les textView
        textViewScoreM = (TextView) findViewById(R.id.textViewScoreH);
        textViewTimeM = (TextView) findViewById(R.id.textViewTimeM);

        //l'action du bouton
        buttonReadyM.setOnClickListener(this);
        buttonEndGameM.setOnClickListener(this);

    }

    public void fight()
    {
        show();
        textViewTimeM.setText("10");
        timeLeft=10;
        button10.setOnClickListener(this);
        button11.setOnClickListener(this);
        button12.setOnClickListener(this);
        button13.setOnClickListener(this);
        button14.setOnClickListener(this);
        button15.setOnClickListener(this);
        button16.setOnClickListener(this);
        button17.setOnClickListener(this);
        button18.setOnClickListener(this);
        button19.setOnClickListener(this);
        button20.setOnClickListener(this);
        button21.setOnClickListener(this);
        button22.setOnClickListener(this);
        button23.setOnClickListener(this);
        button24.setOnClickListener(this);


        button10.setVisibility(View.INVISIBLE);
        button11.setVisibility(View.INVISIBLE);
        button12.setVisibility(View.INVISIBLE);
        button13.setVisibility(View.INVISIBLE);
        button14.setVisibility(View.INVISIBLE);
        button15.setVisibility(View.INVISIBLE);
        button16.setVisibility(View.INVISIBLE);
        button17.setVisibility(View.INVISIBLE);
        button18.setVisibility(View.INVISIBLE);
        button19.setVisibility(View.INVISIBLE);
        button20.setVisibility(View.INVISIBLE);
        button21.setVisibility(View.INVISIBLE);
        button22.setVisibility(View.INVISIBLE);
        button23.setVisibility(View.INVISIBLE);
        button24.setVisibility(View.INVISIBLE);


        buttonReadyM.setEnabled(false);

        hr = new Handler();
        tr = new Thread(){
            @Override
            public void run()
            {
                hr.postDelayed(tr,1000);
                timeLeft--;
                textViewTimeM.setText(String.valueOf(timeLeft));
                if(timeLeft<=0)
                {
                    startGame();
                    hr.removeCallbacks(tr);
                };
            };
        };
        tr.run();
        show();
    }
    public void startGame()
    {

        textViewScoreM.setText(String.valueOf(score));
        button10.setOnClickListener(null);
        button11.setOnClickListener(null);
        button12.setOnClickListener(null);
        button13.setOnClickListener(null);
        button14.setOnClickListener(null);
        button15.setOnClickListener(null);
        button16.setOnClickListener(null);
        button17.setOnClickListener(null);
        button18.setOnClickListener(null);
        button19.setOnClickListener(null);
        button20.setOnClickListener(null);
        button21.setOnClickListener(null);
        button22.setOnClickListener(null);
        button23.setOnClickListener(null);
        button24.setOnClickListener(null);

        buttonReadyM.setEnabled(true);
        setVisibility();
    }


    @Override
    public void onClick(View FIGHT)
    {
        switch(FIGHT.toString()){
            case "button10" : button10.setVisibility(View.INVISIBLE);
                break;
            case "button11" : button11.setVisibility(View.INVISIBLE);
                break;
            case "button12" : button12.setVisibility(View.INVISIBLE);
                break;
            case "button13" : button13.setVisibility(View.INVISIBLE);
                break;
            case "button14" : button14.setVisibility(View.INVISIBLE);
                break;
            case "button15" : button15.setVisibility(View.INVISIBLE);
                break;
            case "button16" : button16.setVisibility(View.INVISIBLE);
                break;
            case "button17" : button17.setVisibility(View.INVISIBLE);
                break;
            case "button18" : button18.setVisibility(View.INVISIBLE);
                break;
            case "button19" : button19.setVisibility(View.INVISIBLE);
                break;
            case "button20" : button20.setVisibility(View.INVISIBLE);
                break;
            case "button21" : button21.setVisibility(View.INVISIBLE);
                break;
            case "button22" : button22.setVisibility(View.INVISIBLE);
                break;
            case "button23" : button23.setVisibility(View.INVISIBLE);
                break;
            case "button24" : button24.setVisibility(View.INVISIBLE);
                break;
        }
        score++;
        show();
        //ajoute les points a chaque click dans le textview du score!
        textViewScoreM.setText(String.valueOf(score));

        //si on pese sur le bouton READY la fonction fight() es lancer
        // sinon le bouton ENd fini le match save les resultas du match dans une arraylist, lactivity finish() et on passe a la page highscore
        if(FIGHT== buttonReadyM)
        {
            fight();
        }
        else if(FIGHT ==  buttonEndGameM){
            endMatchM();
            finish();
            Intent intentGame = new Intent(FIGHT.getContext(), HighScore.class);
            startActivity(intentGame);
        }

    }

    //fonction pour faire un resultas random ALEATOIRE
    public int Randomize(int count)
    {
        int min =0 ;
        int max = 14;
        int randomize;
        do
        {
            randomize = new Random().nextInt((max-min) + 1) + min;
        }while(count == randomize);

        return randomize;
    }

    public void setVisibility()
    {
        //set la visibility a invisible
        button10.setVisibility(View.INVISIBLE);
        button11.setVisibility(View.INVISIBLE);
        button12.setVisibility(View.INVISIBLE);
        button13.setVisibility(View.INVISIBLE);
        button14.setVisibility(View.INVISIBLE);
        button15.setVisibility(View.INVISIBLE);
        button16.setVisibility(View.INVISIBLE);
        button17.setVisibility(View.INVISIBLE);
        button18.setVisibility(View.INVISIBLE);
        button19.setVisibility(View.INVISIBLE);
        button20.setVisibility(View.INVISIBLE);
        button21.setVisibility(View.INVISIBLE);
        button22.setVisibility(View.INVISIBLE);
        button23.setVisibility(View.INVISIBLE);
        button24.setVisibility(View.INVISIBLE);
    }

    public void show()
    {
        //Affiche les boutons avec la fonction RAMDOMIZE
        Random random = new Random();
        setVisibility();
        int randomInt = Randomize(0);
        switch(randomInt){
            case 1 : button10.setVisibility(View.VISIBLE);
                break;
            case 2 : button11.setVisibility(View.VISIBLE);
                break;
            case 3 : button12.setVisibility(View.VISIBLE);
                break;
            case 4 : button13.setVisibility(View.VISIBLE);
                break;
            case 5 : button14.setVisibility(View.VISIBLE);
                break;
            case 6 : button15.setVisibility(View.VISIBLE);
                break;
            case 7 : button16.setVisibility(View.VISIBLE);
                break;
            case 8 : button17.setVisibility(View.VISIBLE);
                break;
            case 9 : button18.setVisibility(View.VISIBLE);
                break;
            case 10 : button19.setVisibility(View.VISIBLE);
                break;
            case 11 : button20.setVisibility(View.VISIBLE);
                break;
            case 12 : button21.setVisibility(View.VISIBLE);
                break;
            case 13 : button22.setVisibility(View.VISIBLE);
                break;
            case 14 : button23.setVisibility(View.VISIBLE);
                break;
            case 15 : button24.setVisibility(View.VISIBLE);
                break;

        }

    };

    public void endMatchM(){
        // 1. create et tu get linfo de sharedpreferences dans une arraylist
        // 2. add le score
        // 3. save and update arraylist avec les sharedpreferences dans la clef data
        ArrayList<String> arrayList;
        SharedPreferences sharedPreferencesScore;

        sharedPreferencesScore = getSharedPreferences("data", MODE_PRIVATE);
        arrayList = readSharedPreferences(sharedPreferencesScore);

        arrayList.add(editTextPersonNameM.getText().toString() + " "
                + textViewScoreM.getText().toString()
                + " Medium difficulty"
                + " Date: " + getDate());
        updatedScoreList(arrayList, sharedPreferencesScore);
    }

    // fonction pour aller chercher la date!
    private String getDate(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-4:00"));
        String dateToString = simpleDateFormat.format(date);
        return dateToString;
    }

    //fonction pour READSHAREDPREFERENCES
    private ArrayList<String> readSharedPreferences(SharedPreferences sharedPreferencesScore){
        ArrayList<String> arrayListTmp = new ArrayList<>();
        Set<String> stringsSet = sharedPreferencesScore.getStringSet("HighScore", null);
        if(stringsSet != null){
            arrayListTmp = new ArrayList<String>(stringsSet);
        }
        return arrayListTmp;
    }

    // update le arraylist contenant les sharedpreferences du telephone
    private void updatedScoreList(ArrayList<String> list, SharedPreferences sharedPreferencesScore){
        try {
            SharedPreferences.Editor editor = sharedPreferencesScore.edit();
            Set<String> setString = new HashSet<>();
            setString.addAll(list);
            editor.putStringSet("HighScore",setString);
            editor.commit();
        }catch (Exception e){
            //Toast.makeText(GameBeginner.this, "ERROR", Toast.LENGTH_SHORT).show();
        }
    }


}